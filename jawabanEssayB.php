<?php
function perolehan_medali($arrayMedals){
    if(count($arrayMedals) == 0){
        return "no data";
    }else{
        $negara = [];
        $medali = [];

        for($i = 0; $i < count($arrayMedals); $i++){
            $negara[] = $arrayMedals[$i][0]; 
        }
        $negara = array_unique($negara);

        foreach ($negara as $kNegara => $vNegara) {
            $emas[$vNegara] = 0;
            $perak[$vNegara] = 0;
            $perunggu[$vNegara] = 0;
            for($i = 0; $i < count($arrayMedals); $i++){
                if($arrayMedals[$i][0] == $vNegara){
                    if($arrayMedals[$i][1] == "emas") $emas[$vNegara]++;
                    if($arrayMedals[$i][1] == "perak") $perak[$vNegara]++;
                    if($arrayMedals[$i][1] == "perunggu") $perak[$vNegara]++;
                }
            }
            $medali[] = array(
                "negara" => $vNegara,
                "emas" => $emas[$vNegara],
                "perak" => $perak[$vNegara],
                "perunggu" => $perunggu[$vNegara]
            );
        }

        return $medali;
    }
    
}

// TEST CASE
echo "<pre>";
print_r (perolehan_medali(
    array(
        array("Indonesia", "emas"),
        array("India", "perak"),
        array("Korea Selatan", "emas"),
        array("India", "perak"),
        array("India", "emas"),
        array("Indonesia", "perak"),
        array("Indonesia", "emas")
    )
    ));
echo "</pre>";

echo "<br>";
echo "<pre>";
print_r(perolehan_medali([]));
echo "</pre>";
?>