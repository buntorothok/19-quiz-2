<?php
function hitung($string_data){
    $operators = ["*","+",":","%","-"];
    $angka = [];
    for($i = 0; $i < count($operators); $i++) {
        $pos = strpos($string_data, $operators[$i]);
        if($pos > 0){
            $operator = $operators[$i];
            $angka = explode($operator, $string_data);
            
            switch ($operator) {
                case ($operator == "*"):
                    echo $angka[0] * $angka[1] ."<br>";
                    break;
                case ($operator == "+"):
                    echo $angka[0] + $angka[1] ."<br>";
                    break;
                case ($operator == ":"):
                    echo $angka[0] / $angka[1] ."<br>";
                    break;
                case ($operator == "%"):
                    echo $angka[0] % $angka[1] ."<br>";
                    break;
                case ($operator == "-"):
                    echo $angka[0] - $angka[1] ."<br>";
                    break;
                default:
                    echo "Format string salah";
                    break;
            }
        }
    }
}

//TEST CASES
echo hitung("102*2");
echo hitung("2+3");
echo hitung("100:25");
echo hitung("10%2");
echo hitung("99-2");
?>