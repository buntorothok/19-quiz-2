SELECT b.name AS customer_name, SUM(a.amount) AS total_amount 
FROM orders a LEFT JOIN customers b ON a.customer_id = b.id 
GROUP BY b.id